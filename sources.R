# producao = FALSE
# if(producao == TRUE){
#   PATH <- "D:\\Prototipacao_Solutions\\Desenvolvimento\\DePara_Automatico\\1.0.0\\DePara_Automatico_Producao\\Programa\\"
# }else{
#   PATH <- "C:\\Users\\gisely.brandao\\Documents\\DePara\\deparaautomatico-Developer\\"
# }

PATH <- "E:\\deparaautomatico-Developer\\"

DIR_TABELA_CENTRALIZADORA <- paste0(PATH,"TabelaCentralizadora")
DIR_BKP = paste0(PATH,"DeParas_BackUp\\")
DIR_BKP_TABELA_CENTRALIZADORA <- paste0(DIR_BKP,"TabelaCentralizadora")
DIR_REVISADOS <- paste0(PATH, "Revisados\\")
DIR_ENTRADA_DADOS <- paste0(PATH, "Entrada de Dados")
DIR_RESULTADO <- paste0(PATH, "Resultado")
DIR_SOBRA <- paste0(PATH, "Sobra")
DIR_BKP_WARNING <- paste0(DIR_BKP,"Warnings")
DIR_BKP_ArquivosOriginais <- paste0(DIR_BKP,"ArquivosOriginais")
DIR_BKP_ArquivosPosScript <- paste0(DIR_BKP,"ArquivosPosScript")